[[Category:Sound]]
[[Category:Audio/Video]]
{{i18n|Advanced Linux Sound Architecture}}

{{Out of date}}

The [[Wikipedia:Advanced Linux Sound Architecture|Advanced Linux Sound Architecture]] ('''ALSA''') is a Linux kernel component intended to replace the original Open Sound System (OSSv3) for providing device drivers for sound cards. Besides the sound device drivers, '''ALSA''' also bundles a user space library for application developers who want to use driver features with a higher level API than direct interaction with the kernel drivers.

{{Note|For an alternative sound environment, see the [[Open Sound System]] page.}}

{{Article summary start}}
{{Article summary text|Using, configuring and troubleshooting ALSA.}}
{{Article summary heading|Related}}
{{Article summary wiki|Sound}}
{{Article summary wiki|Disable PC Speaker Beep}}
{{Article summary end}}

==Installation==
ALSA is included in the default Parabola kernel as a set of modules, so installing it is not necessary.

[[udev]] will automatically probe your hardware at boot, loading the corresponding kernel module for your audio card. Therefore, your sound should already be working, but upstream sources mute all channels by default.

Users must be in the audio [[Users and Groups|group]] to play sound with ALSA. To add a user to the audio group, use the following command:
{{bc|# gpasswd -a ''yourusername'' audio}}

{{Note|This will not take effect until the user logs back in.}}

===User-space utilities===
From [[Official Repositories|official repositories]]:
* [[pacman|Install]] the {{Pkg|alsa-utils}} package which contains the {{ic|alsamixer}} user-space tool, which allows for configuration of the sound device from the console or terminal.
* Install the {{Pkg|alsa-oss}} package if you want OSS applications to work with dmix (software mixing).

{{Note|1=Since [[udev]] >= 171, the OSS emulation modules ({{ic|snd_seq_oss}}, {{ic|snd_pcm_oss}}, {{ic|snd_mixer_oss}}) are not automatically loaded by default.}}

*Install the {{Pkg|alsa-plugins}} package if you want [[#High quality resampling|high quality resampling]], [[#Upmixing/Downmixing|upmixing/downmixing]] and other advanced features.

==Unmuting the channels==
The current version of ALSA installs with all channels '''muted by default'''. You will need to unmute the channels manually.

It is easiest to use {{ic|alsamixer}} ncurses UI to accomplish this (alternatively, use amixer from the commandline):
{{cli|$ alsamixer}}
{{Note|You may have to specify the mixer device, ''i.e.'' {{ic|-D hw:0}}, to see all of the channels.}}

The label {{ic|MM}} below a channel indicates that the channel is muted, and {{ic|00}} indicates that it is open.

Scroll to the {{ic|Master}} and {{ic|PCM}} channels with the {{keypress|←}} and {{keypress|→}} keys and unmute them by pressing the {{keypress|m}} key. Use the {{keypress|↑}} key to increase the volume and obtain a value of {{ic|0}} dB gain. The gain can be found in the upper left next to the {{ic|Item:}} field. Higher values of gain will produce distorted sound.

To get full 5.1 or 7.1 surround sound you likely need to unmute other channels such as Front, Surround, Center, LFE (subwoofer) and Side (these are the names of the channels with Intel HD Audio, they may vary with different hardware). Please take note that this will not automatically upmix stereo sources (like most music). In order to accomplish that, see [[#Upmixing/Downmixing]].

Leave alsamixer by pressing {{Keypress|Esc}}.

{{Note|
* Some cards need to have digital output muted/turned off in order to hear analog sound. For the Soundblaster Audigy LS mute the IEC958 channel.
* Some machines, (like the Thinkpad T61), have a Speaker channel which must be unmuted and adjusted as well.
* Some machines, (like Dell E6400) may also require the {{ic|Front}} and {{ic|Headphone}} channels to be unmuted and adjusted.}}

Next, test to see if sound works:

{{bc|$ speaker-test -c 2}}

Change -c to fit your speaker setup. Use -c 8 for 7.1, for instance:

{{bc|$ speaker-test -c 8}}

If it does not work, proceed to [[#Configuration]] and then [[#Troubleshooting]] to solve your issues.

If it works, you just need to save your mixer settings.

{{bc|# alsactl store}}

This will create the file {{ic|/var/lib/alsa/asound.state}}, saving the alsamixer settings.

Then, add the {{ic|alsa}} [[daemon]] to your {{ic|DAEMONS}} array in {{ic|/etc/rc.conf}} to automatically restore the mixer settings at boot. See [[rc.conf#Daemons]] for detailed instructions.

{{Note|The {{ic|alsa}} daemon merely restores your volume mixer levels on boot up by reading {{ic|/var/lib/alsa/asound.state}}. It is separate from the alsa audio library (and kernel level API).}}

==Configuration==

===Set the default sound card===
First you will have to find out the card and device id that you want to set as the default by running {{ic|aplay -l}}:
{{hc|$ aplay -l|2=<nowiki>
 Subdevice #0: subdevice #0
</nowiki>}}

For example, the last entry in this list has the card id 2 and the device id 0. To set this card as the default, you can either use the system-wide file {{ic|/etc/asound.conf}} or the user-specific file {{ic|~/.asoundrc}}. You may have to create the file if it does not exist. Then insert the following options with the corresponding card and device id.
{{hc|~/.asoundrc|2=<nowiki>
defaults.pcm.card 2
defaults.pcm.device 0
defaults.ctl.card 2
</nowiki>}}
The 'pcm' options affect which card and device will be used for audio playback while the 'ctl' option affects which card is used by control utilities like alsamixer .

The changes should take effect as soon as you (re-)start an application (mplayer etc.).


===Making sure the sound modules are loaded===
You can assume that udev will autodetect your sound properly. You can check this with the command

{{hc|<nowiki>$ lsmod | grep '^snd' | column -t</nowiki>|<nowiki>
snd_hda_codec_hdmi     22378   4
snd_hda_codec_realtek  294191  1
snd_hda_intel          21738   1
snd_hda_codec          73739   3  snd_hda_codec_hdmi,snd_hda_codec_realtek,snd_hda_intel
snd_hwdep              6134    1  snd_hda_codec
snd_pcm                71032   3  snd_hda_codec_hdmi,snd_hda_intel,snd_hda_codec
snd_timer              18992   1  snd_pcm
snd                    55132   9  snd_hda_codec_hdmi,snd_hda_codec_realtek,snd_hda_intel,snd_hda_codec,snd_hwdep,snd_pcm,snd_timer
snd_page_alloc         7017    2  snd_hda_intel,snd_pcm
</nowiki>}}

If the output looks similar, your sound drivers have been successfully autodetected.
{{Note|Since {{ic|udev>&#61;171}}, the OSS emulation modules ({{ic|snd_seq_oss, snd_pcm_oss, snd_mixer_oss}}) are not loaded by default: If they are needed, load them with {{ic|modprobe}} and/or add them in the {{ic|MODULES}} array in [[rc.conf|{{ic|/etc/rc.conf}}]]. As they bypass software mixing, don't load them unless you have legacy software that supports nothing but OSS.}}

You might also want to check the directory '''/dev/snd''' for the right device files:

{{hc|$ ls -l /dev/snd|2=<nowiki>
total 0
crw-rw----  1 root audio 116,  0 Apr  8 14:17 controlC0
crw-rw----  1 root audio 116, 32 Apr  8 14:17 controlC1
crw-rw----  1 root audio 116, 24 Apr  8 14:17 pcmC0D0c
crw-rw----  1 root audio 116, 16 Apr  8 14:17 pcmC0D0p
crw-rw----  1 root audio 116, 25 Apr  8 14:17 pcmC0D1c
crw-rw----  1 root audio 116, 56 Apr  8 14:17 pcmC1D0c
crw-rw----  1 root audio 116, 48 Apr  8 14:17 pcmC1D0p
crw-rw----  1 root audio 116,  1 Apr  8 14:17 seq
crw-rw----  1 root audio 116, 33 Apr  8 14:17 timer
</nowiki>}}

{{Note|If requesting help on IRC or the forums, please post the output of the above commands.}}

If you have at least the devices '''controlC0''' and '''pcmC0D0p''' or similar, then your sound modules have been detected and loaded properly.

If this is not the case, your sound modules have not been detected properly. To solve this, you can try loading the modules manually:

* Locate the module for your soundcard: [http://www.alsa-project.org/main/index.php/Matrix:Main ALSA Soundcard Matrix] The module will be prefixed with 'snd-' (for example: 'snd-via82xx').
* Load modules:
  # modprobe snd-NAME-OF-MODULE
* Check for the device files in '''/dev/snd''' (see above) and/or try if '''alsamixer''' or '''amixer''' have reasonable output.
* Add '''snd-NAME-OF-MODULE''' and '''snd-pcm-oss''' to the list of MODULES in {{ic|/etc/rc.conf}} to ensure they are loaded next time (make sure '''snd-NAME-OF-MODULE''' is before '''snd-pcm-oss''').

===Restore ALSA Mixer settings at startup===
{{Warning|If you use {{ic|kmix}}, make sure to configure it to not restore sound levels at startup. This will conflict with the configuration detailed below.}}
* Run {{ic|alsactl -f /var/lib/alsa/asound.state store}} once to create {{ic|/var/lib/alsa/asound.state}}.

 # alsactl -f /var/lib/alsa/asound.state store

* Edit {{ic|/etc/[[rc.conf]]}} and add {{ic|"alsa"}} to the list of daemons to start on boot-up. This will store the mixer settings on every shutdown and restore them when you boot.

* If the mixer settings are not loaded on boot-up, add the following line to {{ic|/etc/rc.local}}:

 # alsactl -f /var/lib/alsa/asound.state restore

* These methods still may not work, or you may prefer to have audio settings for individual users.  In this case, run {{ic|alsactl store -f ~/.asoundrc}} as a normal user.  This will save and restore volume settings on a per user basis.  To automate this process, add the respective commands to {{ic|~/.bash_login}} and {{ic|~/.bash_logout}}, or the correct locations for the shell of your choice.

For zsh, use {{ic|~/.zlogin}} and {{ic|~/.zlogout}}.

===Getting SPDIF Output===
(from gralves from the Gentoo forums)
* In GNOME Volume Control, under the Options tab, change the IEC958 to PCM.  This option can be enabled in the preferences.
* If you do not have GNOME Volume Control installed,
** Edit /var/lib/alsa/asound.state.  This file is where alsasound stores your mixer settings.
** Find a line that says: 'IEC958 Playback Switch'.  Near it you will find a line saying value:false. Change it to value:true.
** Now find this line: 'IEC958 Playback AC97-SPSA'. Change its value to 0.
** Restart ALSA.

Alternative way to enable SPDIF output automatically on login (tested on SoundBlaster Audigy):

* add following lines to {{ic|/etc/rc.local}}:
  # Use COAX-digital output
  amixer set 'IEC958 Optical' 100 unmute
  amixer set 'Audigy Analog/Digital Output Jack' on

You can see the name of your card's digital output with:
  $ amixer scontrols

===System-Wide Equalizer===

====Using mbeq====
{{Note | This method requires the use of a ladspa plugin which might use quite a bit of CPU when sound plays. In addition, this was made with stereophonic sound (e.g. headphones) in mind.}}

Install the {{Pkg|alsa-plugins}}, {{Pkg|ladspa}} and {{Pkg|swh-plugins}} packages if you do not already have them.

* If you have not already created either an {{ic|~/.asoundrc}} or a {{ic|/etc/asound.conf}} file, then create either one and insert the following:

{{hc|/etc/asound.conf|2=<nowiki>pcm.eq {
  type ladspa

  # The output from the EQ can either go direct to a hardware device
  # (if you have a hardware mixer, e.g. SBLive/Audigy) or it can go
  # to the software mixer shown here.
  #slave.pcm "plughw:0,0"
  slave.pcm "plug:dmix"

  # Sometimes you may need to specify the path to the plugins,
  # especially if you have just installed them.  Once you have logged
  # out/restarted this should not be necessary, but if you get errors
  # about being unable to find plugins, try uncommenting this.
  #path "/usr/lib/ladspa"

  plugins [
    {
      label mbeq
      id 1197
      input {
        #this setting is here by example, edit to your own taste
        #bands: 50hz, 100hz, 156hz, 220hz, 311hz, 440hz, 622hz, 880hz, 1250hz, 1750hz, 25000hz,
        #50000hz, 10000hz, 20000hz
        controls [ -5 -5 -5 -5 -5 -10 -20 -15 -10 -10 -10 -10 -10 -3 -2 ]
      }
    }
  ]
 }

 # Redirect the default device to go via the EQ - you may want to do
 # this last, once you're sure everything is working.  Otherwise all
 # your audio programs will break/crash if something has gone wrong.

 pcm.!default {
  type plug
  slave.pcm "eq"
 }

 # Redirect the OSS emulation through the EQ too (when programs are running through "aoss")

 pcm.dsp0 {
  type plug
  slave.pcm "eq"
 }
</nowiki>}}

* Reload your alsa settings (as root).
 # rc.d restart alsa

* You should be good to go (if not, ask in the forum).

==High quality resampling==
When software mixing is enabled, ALSA is forced to resample everything to the same frequency (48000 by default when supported). dmix uses a poor resampling algorithm which produces noticeable sound quality loss.

Install alsa-plugins and libsamplerate:
{{bc|# pacman -S alsa-plugins libsamplerate}}

Change the default rate converter to libsamplerate:

{{hc|/etc/asound.conf|defaults.pcm.rate_converter "samplerate_best"}}

or

{{hc|~/.asoundrc|defaults.pcm.rate_converter "samplerate_best"}}

'''samplerate_best''' offers the best sound quality, but you need a decent CPU to be able to use it as it requires a lot of CPU cycles for real-time resampling. There are other algorithms available ('''samplerate''', etc.) but they may not provide much of an improvement over the default resampler.

==Upmixing/Downmixing==
===Upmixing===
In order for stereo sources like music to be able to saturate a 5.1 or 7.1 sound system, you need to use upmixing. In darker days this used to be tricky and error prone but nowadays plugins exist to easily take care of this task. Thus, install {{Pkg|alsa-plugins}}.

Then add the following to your alsa configuration file of choice (either /etc/asound.conf or ~/.asoundrc):
 pcm.upmix71 {
     type upmix
     slave.pcm "surround71"
     delay 15
     channels 8
 }

You can easily change this example for 7.1 upmixing to 5.1 or 4.0.

This adds a new pcm that you can use for upmixing. If you want all sound sources to go through this pcm, add it as a default below the previous definition like so:

 pcm.!default "plug:upmix71"

The plugin automatically allows multiple sources to play through it without problems so setting is as a default is actually a safe choice.
If this is not working, you have to setup your own dmixer for the upmixing PCM like this: 
 pcm.dmix6 {
     type asym
     playback.pcm {
         type dmix
         ipc_key 567829
         slave {
             pcm "hw:0,0"
             channels 6
         }
     }
 }

and use "dmix6" instead of "surround71".

If you experience skipping or distorted sound, consider increasing the buffer_size (to 32768, for example) or use a [[#High quality resampling|high quality resampler]].

===Downmixing===
If you want to downmix sources to stereo because you, for instance, want to watch a movie with 5.1 sound on a stereo system, you need to use the vdownmix plugin that alsa provides in the same package.

Again, in your configuration file, add this:

 pcm.!surround51 {
     type vdownmix
     slave.pcm "default"
 }
 pcm.!surround40 {
     type vdownmix
     slave.pcm "default"
 }

==Mixing==
===Software mixing (dmix)===

{{Note|For ALSA 1.0.9rc2 and higher on analog sound outputs you do not need to setup dmix. Dmix is enabled as default for soundcards which do not support hardware mixing.}}

If that does not work however, it is a matter of simply creating a .asoundrc file in your home folder with the following contents.

 pcm.dsp {
     type plug
     slave.pcm "dmix"
 }

This should enable software mixing and allows more than one application to make use of the soundcard. 

For a digital sound output such as S/PDIF, the ALSA package still does not enable dmix by default. Thus, the dmix configuration above can be used to enable dmix for S/PDIF devices.

See [[#Troubleshooting]] for common problems and solutions.

===Hardware mixing===
{{poor writing}}
*Use a soundcard that supports hardware mixing: old SoundBlaster Live! series (emu10k1 chip) and new Audigy series (emu10k2 chip) cards are known to work.

*The SoundBlaster Live! 5.1 is fairly easy to find and is cheap to buy.

{{Note|new cheap Audigy SE, Audigy 2 NX, SoundBlaster Live! 24bit and SoundBlaster Live! 7.1 '''do not''' support hardware mixing (they use other chips).}}

*99% of motherboards with integrated sound '''do not''' support hardware mixing.

{{Note|An exception is the onboard VIA8237 chip which supports 4-stream hardware mixing. However it does only 3 for some motherboards (the 4th makes no sound), or is just broken. Even if it works, the quality is not good compared to other solutions.}}

*For Parabola 64 bits and the 00:1b.0 Audio device: Intel Corporation 82801I (ICH9 Family) HD Audio Controller (rev 02), and to get sound from Enemy Territory, it works this by setting these:
 echo "et.x86 0 0 direct" > /proc/asound/card0/pcm0p/oss
 echo "et.x86 0 0 disable" > /proc/asound/card0/pcm0c/oss

==Troubleshooting==
===Sound Skipping While Using Dynamic Frequency Scaling===
Some combinations of ALSA drivers and chipsets may cause audio from all sources to skip when used in combination with a dynamic frequency scaling governor such as {{ic|ondemand}} or {{ic|conservative}}. Currently, the solution is to switch back to the {{ic|performance}} governor.

Refer to the [[CPU Frequency Scaling]] for more information.

===Problems with Availability to Only One User at a Time===
You might find that only one user can use the dmixer at a time.  This is probably ok for most, but for those who run [[mpd]] as a separate user this poses a problem.  When mpd is playing a normal user cannot play sounds though the dmixer.  While it's quite possible to just run mpd under a user's login account, another solution has been found.  Adding the line {{ic|ipc_key_add_uid 0}} to the {{ic|pcm.dmixer}} block disables this locking.  The following is a snippet of the {{ic|asound.conf}}, the rest is the same as above.
 ...
 pcm.dmixer {
  type dmix
  ipc_key 1024
  ipc_key_add_uid 0
  ipc_perm 0660
 slave {
 ...

===Random Lack of Sound on Startup===
If you randomly have no sound on startup, it may be because your system has multiple sound cards, and their order may sometimes change on startup.  If this is the case, then change this section of {{ic|/etc/asound.conf}}:

{{bc|<nowiki>
ctl.dmixer {
    type hw
    card FOO
}
</nowiki>}}
Replace FOO with the desired audio device, as reported in the /proc/asound/cards file.  An example of the file is shown below.
{{bc|
 0 [U0x46d0x9a1    ]: USB-Audio - USB Device 0x46d:0x9a1
                      USB Device 0x46d:0x9a1 at usb-0000:00:12.2-2, high speed
 1 [SB             ]: HDA-Intel - HDA ATI SB
                      HDA ATI SB at 0xf9ff4000 irq 16
}}
Device 0 is the microphone built into a webcam, while device 1 is the integrated sound card.  If you have copied the {{ic|/etc/asound.conf}} from above as is, alsa will attempt to initialize the microphone as an audio output device, but will fail and you will have no sound.  Rather than setting FOO to the number, you set it to the name next to the number, like so:
{{bc|<nowiki>
ctl.dmixer {
    type hw
    card SB
}
</nowiki>}}

If you use dmix, you will want to have a configuration such as this one:
{{bc|<nowiki>
ctl.!default {
    type hw
    card Intel #adapt this to your card
    #device 0 #not required, defaults to 0 (see the output of aplay -L)
}

pcm.dmixer {
    type dmix
    ipc_key 1024 #required and must be unique
    slave.pcm {
        type hw
        card Intel #same as above
        #device 0 #same as above
    }
}

pcm.!default {
    type plug
    slave.pcm dmixer
}
</nowiki>}}

To make most programs use Alsa, make sure {{ic|/etc/libao.conf}} only contains the following.
 default_driver=alsa10

If you use mpd and the configuration tips above do not work for you, try [http://mpd.wikia.com/wiki/Configuration#ALSA_MPD_software_volume_control reading this] instead.

===Specific Program Problems===
For other programs who insist on their own audio setup, eg, XMMS or Mplayer, you would need to set their specific options.

For mplayer, open up {{ic|~/.mplayer/config}} (or {{ic|/etc/mplayer/mplayer.conf}} for global setting) and add the following line:
 ao=alsa

For XMMS/Beep Media Player, go into their options and make sure the sound driver is set to Alsa, not oss.

To do this in XMMS:
* Open XMMS
** Options -> preferences.
** Choose the Alsa output plugin.

For applications which do not provide a ALSA output, you can use aoss from the alsa-oss package. To use aoss, when you run the program, prefix it with {{ic|aoss}}, eg:
 aoss realplay

pcm.!default{ ... }  doesnt work for me anymore. but this does:
  pcm.default pcm.dmixer

===Model Settings===
Although Alsa detects your soundcard through the BIOS at times Alsa may not be able to recognize your [http://www.kernel.org/doc/Documentation/sound/alsa/HD-Audio-Models.txt model type].  The soundcard chip can be found in {{ic|alsamixer}} (e.g. ALC662) and the model can be set in {{ic|/etc/modprobe.d/modprobe.conf}} or {{ic|/etc/modprobe.d/sound.conf}}.  For example:

 options snd-hda-intel model=MODEL

There are other model settings too.  For most cases Alsa defaults will do.  If you want to look at more specific settings for your soundcard take a look at the [http://bugtrack.alsa-project.org/main/index.php/Matrix:Main Alsa Soundcard List] find your model, then Details, then look at the "Setting up modprobe..." section.  Enter these values in {{ic|/etc/modprobe.d/modprobe.conf}}.  For example, for an Intel AC97 audio:

{{bc|# ALSA portion
alias char-major-116 snd
alias snd-card-0 snd-intel8x0
# module options should go here

# OSS/Free portion
alias char-major-14 soundcore
alias sound-slot-0 snd-card-0

# card #1
alias sound-service-0-0 snd-mixer-oss
alias sound-service-0-1 snd-seq-oss
alias sound-service-0-3 snd-pcm-oss
alias sound-service-0-8 snd-seq-oss
alias sound-service-0-12 snd-pcm-oss}}

===Conflicting PC Speaker===
If you're sure nothing is muted, that your drivers are installed correctly, and that your volume is right, but you still do not hear anything, then try adding one of the following lines to {{ic|/etc/modprobe.d/modprobe.conf}}:

 options snd-NAME-OF-MODULE ac97_quirk=0

The above fix has been observed to work with {{ic|via82xx}}
 options snd-NAME-OF-MODULE ac97_quirk=1
The above fix has been reported to work with {{ic|snd_intel8x0}}

===No Microphone Input===
In alsamixer, make sure that all the volume levels are up under recording, and that CAPTURE is toggled active on the microphone (e.g. Mic, Internal Mic) and/or on Capture (in alsamixer, select these items and press space). Try making positive Mic Boost and raising Capture and Digital levels higher; this make make static or distortion, but then you can adjust them back down once you're hearing ''something'' when you record

As the pulseaudio wrapper is shown as "default" in alsamixer, your may have to press F6 to select your actual soundcard first. You may also need to enable and increase the volume of Line-in in the Playback section.

To test the microphone, run these commands (see arecord's man page for further information):
  arecord -d 5 test-mic.wav
  aplay test-mic.wav

If all fails, you may want to eliminate hardware failure by testing the microphone with a different device.

For at least some computers, muting a microphone (MM) simply means its input doesn't go immediately to the speakers. It still receives input.

Many Dell laptops need "-dmic" to be appended to the model name in {{ic|/etc/modprobe.d/modprobe.conf}}:
  options snd-hda-intel model=dell-m6-dmic

Some programs use try to use OSS as the main input software. Add the following lines to {{ic|/etc/modprobe.d/modprobe.conf}} to prevent OSS modules from being loaded:

{{Note|The OSS modules are no longer autoloaded anyway.}}

 blacklist snd_pcm_oss
 blacklist snd_mixer_oss
 blacklist snd_seq_oss

See also:
* http://www.alsa-project.org/main/index.php/SoundcardTesting
* http://alsa.opensrc.org/Record_from_mic

===Setting the default Microphone/Capture Device===
Some applications (Pidgin) do not provide an option to change the capture device. It becomes an issue if your microphone is on a separate device (i.e. USB webcam or microphone) than your internal sound card. To change only the default capture device, leaving the default playback device as is, you can modify your ~/.asoundrc file to include the following:

 pcm.usb
 {
     type hw
     card U0x46d0x81d
 }
 
 pcm.!default
 {
     type asym
     playback.pcm
     {
         type plug
         slave.pcm "dmix"
     }
     capture.pcm
     {
         type plug
         slave.pcm "usb"
     }
 }

Replace "U0x46d0x81d" with your capture device's card name in ALSA. You can use 'arecord -L' to list all the capture devices detected by ALSA.

===Internal Microphone not working===
First make sure all the volume levels are up under recording in alsamixer. In my case adding the following option to /etc/sound.conf and reloading the snd-* module produced a new volume setting called Capture which was capturing for the internal mic. For eg, for snd-hda-intel add

  options snd-hda-intel enable_msi=1

Then reload the module (as below), up the recording volume of Capture and then test.

{{bc|# rmmod snd-hda-intel && modprobe snd-hda-intel}}

===No Sound with Onboard Intel Sound Card===
There may be an issue with two conflicting modules loaded, namely {{ic|snd_intel8x0}} and {{ic|snd_intel8x0m}}. In this case, blacklist snd_intel8x0m:

{{hc|/etc/modprobe.d/modprobe.conf|blacklist snd_intel8x0m}}

''Muting'' the "External Amplifier" in {{ic|alsamixer}} or {{ic|amixer}} may also help. See [http://alsa.opensrc.org/Intel8x0#Dell_Inspiron_8600_.28and_probably_others.29 the ALSA wiki].

===No Headphone Sound with Onboard Intel Sound Card===
With '''Intel Corporation 82801 I (ICH9 Family) HD Audio Controller''' on laptop, you may need to add this line to modprobe or sound.conf:

 options snd-hda-intel model=$model

Where $model is any one of the following (in order of possibility to work, but not merit):

* dell-vostro
* olpc-xo-1_5
* laptop
* dell-m6
* laptop-hpsense

Note: It may be necessary to put this "options" line below (after) any "alias" lines about your card.

You can see all the available models in the kernel documentation. For example [http://git.kernel.org/?p=linux/kernel/git/stable/linux-2.6.35.y.git;a=blob;f=Documentation/sound/alsa/HD-Audio-Models.txt;h=dc25bb84b83b49665a7ed850e7bf5423d50cd3ba;hb=HEAD here], but check that it is the correct version of that document for your kernel version.

A list of available models is also available [http://www.mjmwired.net/kernel/Documentation/sound/alsa/HD-Audio-Models.txt here]. To know your chip name type the following command (with * being corrected to match your files). Note that some chips could have been renamed and don't directly match the available ones in the file.

 cat /proc/asound/card*/codec* | grep Codec

Note that there is a high chance none of the input devices (all internal and external mics) will work if you choose to do this, so it is either your headphones or your mic. Please report to ALSA if you are affected by this bug.

And also, if you have problems getting beeps to work (pcspkr):

 options snd-hda-intel model=$model enable=1 index=0

===No sound when S/PDIF video card is installed===
Discover available modules and their order:
 $ cat /proc/asound/modules
 0 snd_hda_intel
 1 snd_ca0106

Disable the undesired video card audio codec in {{ic|/etc/modprobe.d/modprobe.conf}}:
 # /etc/modprobe.d/modprobe.conf
 #
 install snd_hda_intel /bin/false

If both devices use the same module, it might be possible to disable one of them in the BIOS.

===Poor sound quality or clipping===
If you experience poor sound quality, try setting the PCM volume (in alsamixer) to a level such that gain is 0.

===Pops When Starting and Stopping Playback===
Some modules (i.e. snd-ac97-codec and snd-hda-intel) can power off your sound card when not in use. This can make an audible noise (like a crack/pop/scratch) when turning on/off your sound card. If you find this annoying try "modinfo snd-MY-MODULE", and look for a module option that adjusts or disables this feature.

''Example:'' to disable the power saving mode using snd-hda-intel add "options snd-hda-intel power_save=0" in /etc/modprobe.d/modprobe.conf. You can also try it with "modprobe snd-hda-intel power_save=0".

You may also have to unmute the 'Line' alsa channel for this to work. Any value will do (other than '0' or something too high).

''Example:'' on an onboard VIA VT1708S (using the snd-hda-intel module) these cracks occured even though 'power_save' was set to 0. Unmuting the 'Line' channel and setting a value of '1' solved the issue.

Source: http://www.kernel.org/doc/Documentation/sound/alsa/powersave.txt

===Alsamixer Does Not Run===
If running alsamixer does not work and you wind up with the following error
 alsamixer: function snd_ctl_open failed for default: No such device or directory

You should first check /etc/group to ensure that your current user is in the 'audio' group. Do not forget to log out and log in again for the group changes.

Then you might need to re-install your kernel. Run 'pacman -S linux' or whichever patchset you prefer to use.

===S/PDIF Output Does Not Work===

If the optical/coaxial digital output of your motherboard/sound card is not working or stopped working, and have already enabled and unmuted it in alsamixer, try running
 # iecset audio on

as root.

You can also put this command in {{Ic|rc.local}} as it sometimes it may stop working after a reboot.

===HDMI Output Does Not Work===
If the HDMI output of your motherboard/sound card is not working or stopped working, and have already enabled and unmuted it in alsamixer, try the following.

Query for Playback Devices:

  $ aplay -l
  **** List of PLAYBACK Hardware Devices ****
  card 0: NVidia [HDA NVidia], device 0: ALC1200 Analog [ALC1200 Analog]
    Subdevices: 1/1
    Subdevice #0: subdevice #0
  card 0: NVidia [HDA NVidia], device 1: ALC1200 Digital [ALC1200 Digital]
    Subdevices: 1/1
    Subdevice #0: subdevice #0
  card 0: NVidia [HDA NVidia], device 3: NVIDIA HDMI [NVIDIA HDMI]
    Subdevices: 0/1
    Subdevice #0: subdevice #0

Now that we have the info for the HDMI device, try a test,  In the example below, 0 is the card number and 3 is the device number.

  $ aplay -D plughw:0,3 /usr/share/sounds/alsa/Front_Center.wav

If aplay does not output any errors, but still no sound is heared, "reboot" the receiver, monitor or tv set. Since the HDMI interface executes a handshake on connection, it might have noticed before that there was no audio stream embedded, and disabled audio decoding.

{{Note|If you're using an ATI card and linux 3.0, check your kernel/grub settings, you most likely need to use the ''''radeon.audio'''<nowiki>=</nowiki>'''1'''' boot parameter, or alternatively set up the option in your modprobe configuration files.}}

If the test is successful, create or edit your ~/.asoundrc file to set HDMI as the default audio device.

  pcm.!default {
       type hw
       card 0
       device 3
  }

You can restart alsa with the example below or you may reboot, and audio should now work.
{{Note|Any programs running that use alsa will be closed.}}
 # rc.d force-restart alsa

====Using udev to automatically turn HDMI audio on or off====
The previous steps in this section are good for testing, or if you don't need to hotplug your HDMI cable often. We now consider the situation where you have a laptop and want HDMI audio only when you have the HDMI cable plugged in.

The code in this section comes from [https://bbs.archlinux.org/viewtopic.php?id=132641 The Arch Linux Forums].

We create a udev rule (for processing the cable connect/disconnect events), two different ALSA configuration files, and a script that switches between these configurations.

First create {{ic|/etc/alsa/}} if it doesn't exist yet; it will house the configuration files and the switcher script:

{{bc|<nowiki>[[ -d /etc/alsa/ ]] || sudo mkdir -v /etc/alsa</nowiki>}}

Place inside this directory the following files (which you may wish to tweak as you see fit):

{{hc|/etc/alsa/hdmi-switch|2=
<nowiki>
#! /bin/bash
set -x

udevadm settle --quiet --timeout=16

read hdmi_status </sys/class/drm/card0-HDMI-A-1/status
alsa_dir=/etc/alsa
asound_file=asound.hdmi-$hdmi_status

if [[ -n $hdmi_status && -f "$alsa_dir/$asound_file" ]]; then
    ln -sf "$alsa_dir/$asound_file" /etc/asound.conf
    /etc/rc.d/alsa force-restart # or restart if you don't want existing clients to be unceremoniously shut down
fi
</nowiki>
}}

{{hc|/etc/alsa/asound.hdmi-connected|2=
pcm.!default {
    type plug
    slave.pcm "dmixer"
}

pcm.dsp0 {
    type plug
    slave.pcm "dmixer"
}

pcm.dmixer {
    type dmix
    ipc_key 1024             
    ipc_key_add_uid 0
    ipc_perm 0666                
    slave {
        pcm "hw:0,3"    # HDMI CARD AND DEVICE
        period_time 0
        period_size 1024
        buffer_size 8192
        rate 48000 #or 44100
    }   
}

ctl.dmixer {
    type hw
    card 0
}
}}

{{hc|/etc/alsa/asound.hdmi-disconnected|2=
pcm.!default {
   type plug
   slave.pcm "dmixer"
}
  
pcm.dsp0 {
   type plug
   slave.pcm "dmixer"
}
  
pcm.dmixer {
   type dmix
   ipc_key 1024                
   ipc_key_add_uid 0
   ipc_perm 0666           
   slave {
       pcm "hw:0,0"      # LOCAL CARD AND DEVICE
       period_time 0
       period_size 1024
       buffer_size 8192
       rate 48000 #or 44100
   }
}
  
ctl.dmixer {
   type hw
   card 0
}

}}

The two asound.conf versions include dmix settings and allow merging of user permissions so that, for example, the mpd user and your regular user can access it at the same time.

You may now test that the switcher script works by running,

 # chmod +x /etc/alsa/hdmi-switch
 # /etc/alsa/hdmi-switch

You should find that you can successfully obtain HDMI audio output when you run the script while the cable is plugged in. You should be able to restore your default audio output by unplugging the cable and manually running the script again.

Once you have that working, we automate the process by adding the udev rule:

{{hc|/etc/udev/rules.d/hdmi.rules|2=
SUBSYSTEM=="drm", ACTION=="change", RUN+="/bin/bash /etc/alsa/hdmi-switch"
}}

To make udev incorporate the new rule immediately, reload all rules:

 # udevadm control --reload-rules

===HDMI Multi-channel PCM output does not work (Intel) ===
As of Linux 3.1 multi-channel PCM output through HDMI with a Intel card (Intel Eaglelake, IbexPeak/Ironlake,SandyBridge/CougarPoint and IvyBridge/PantherPoint) is not yet supported. Support for it has been recently added and expected to be available in Linux 3.2. To make it work in Linux 3.1 you need to apply the following patches:

* [http://git.kernel.org/?p=linux/kernel/git/torvalds/linux.git;a=patch;h=76adaa34db407f174dd06370cb60f6029c33b465 drm: support routines for HDMI/DP ELD]
* [http://git.kernel.org/?p=linux/kernel/git/torvalds/linux.git;a=patch;h=e0dac65ed45e72fe34cc7ccc76de0ba220bd38bb drm/i915: pass ELD to HDMI/DP audio driver]

===No Adjustable PCM Channel===
You may find that you lack adjustable PCM channel. In this case try to remove all sound-related stuff from MODULES section in /etc/rc.conf, except for snd-NAME-OF-MODULE.

===HP TX2500===
Add these 2 lines into {{ic|/etc/modprobe.d/modprobe.conf}}:
 options snd-cmipci mpu_port=0x330 fm_port=0x388
 options snd-hda-intel index=0 model=toshiba position_fix=1

 options snd-hda-intel model=hp (works for tx2000cto)

===Skipping Sound When Playing MP3===
If you have sound skipping when playing MP3 files and you have more then 2 speakers attached to your computer (i.e. > 2 speaker system), run alsamixer and disable the channels for the speakers that you '''DO NOT''' have (i.e. do not enable the sound for the center speaker if you do not have a center speaker.

===Using a USB Headset and External USB Sound Cards===
If you are using a USB headset with ALSA you can try using asoundconf (currently only available from the AUR) to set the headset as the primary sound output. ''note: before running please make sure you have usb audio module enabled
 #modprobe snd-usb-audio

you can add this to /etc/rc.conf if you wish''

 # asoundconf is-active
 # asoundconf list
 # asoundconf set-default-card <chosen soundcard>

==== Crackling sound with USB devices ====
If you experience crackling sound on USB devices you can try tunning the snd-usb-audio for minimal latency.

Add this to your /etc/modprobe.d/modprobe.conf :

 options snd-usb-audio nrpacks=1

source : http://alsa.opensrc.org/Usb-audio#Tuning_USB_devices_for_minimal_latencies

===Error 'Unkown hardware' Appears After a Kernel Update===
The following messages may be displayed during the start-up ALSA after the kernel update:
 Unknown hardware "foo" "bar" ...
 Hardware is initialized using a guess method
 /usr/sbin/alsactl: set_control:nnnn:failed to obtain info for control #mm (No such file or directory)

or
 Found hardware: "HDA-Intel" "VIA VT1705" "HDA:11064397,18490397,00100000" "0x1849" "0x0397"
 Hardware is initialized using a generic method
 /usr/sbin/alsactl: set_control:1328: failed to obtain info for control #1 (No such file or directory)
 /usr/sbin/alsactl: set_control:1328: failed to obtain info for control #2 (No such file or directory)
 /usr/sbin/alsactl: set_control:1328: failed to obtain info for control #25 (No such file or directory)
 /usr/sbin/alsactl: set_control:1328: failed to obtain info for control #26 (No such file or directory)

Simply store ALSA mixer settings again (as root):
 # alsactl -f /var/lib/alsa/asound.state store

It may be necessary configure alsa again with alsamixer

===HDA Analyzer===
If the mappings to your audio pins(plugs) do not correspond but ALSA works fine, you could try HDA Analyzer -- a pyGTK2 GUI for HD-audio control can be found [http://www.alsa-project.org/main/index.php/HDA_Analyzer at the ALSA wiki].
Try tweaking the Widget Control section of the PIN nodes, to make microphones IN and headphone jacks OUT. Referring to the Config Defaults heading is a good idea.

NOTE: the script is done by such way that it is incompatible with python3 (which is now shipped with ArchLinux) but tries to use it.
The workaround is: open "run.py", find all occurences of "python" (2 occurences - one on the first line, and the second on the last line) and replace them all by "python2".

NOTE2: the script requires root acces, but running it via su/sudo is bogus. Run it via kdesu or gtksu.

===ALSA with SDL===
If you get no sound via SDL and ALSA cannot be chosen from the application. Try setting the environmental variable SDL_AUDIODRIVER to alsa.
 # export SDL_AUDIODRIVER=alsa

==Example configurations==
See [[Advanced Linux Sound Architecture/Example Configurations]].

==See also==
* [http://www.mjmwired.net/kernel/Documentation/sound/alsa/ALSA-Configuration.txt Advanced ALSA module configuration]
* [http://alsa.opensrc.org/Main_Page Unofficial ALSA Wiki]
* [http://bbs.archlinux.org/viewtopic.php?id=36815 HOWTO: Compile driver from svn]

==Acknowledgement==
{{Acknowledgement}}