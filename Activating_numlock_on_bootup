[[Category:Boot process]]
[[Category:Desktop environments]]
[[Category:Keyboards]]
{{i18n|Activating numlock on bootup}}

{{Out of date}}

== TTY (Teletype) Consoles 1-6 ==

To activate NumLock during normal bootup in TTY consoles 1-6 (tty1 -> tty6), add the following line to {{ic|/etc/rc.local}}:
 for tty in /dev/tty?; do /usr/bin/setleds -D +num < "$tty"; done

If strange things start to happen (the NumLock LED is on, but the num pad still works as arrow keypad), there seems to be a conflict between {{Ic|setleds}} and Xserver.
Limit the {{Ic|for}} command only to the consoles you have set on {{ic|/etc/inittab}}. For example, for the first 6 consoles (default) : 
 for tty in /dev/tty{1..6}; do ...

== X.org ==
If you use {{Ic|startx}} to start your X session, simply install the {{Pkg|numlockx}} package and add it to your {{ic|~/.xinitrc}} file.

Install {{Pkg|numlockx}}:
 # pacman -S numlockx

Add it to {{ic|~/.xinitrc}} before {{Ic|exec}}:
 #!/bin/sh
 #
 # ~/.xinitrc
 #
 # Executed by startx (run your window manager from here)
 #
 
 numlockx &
 
 exec your_window_manager

=== KDM ===
If you use KDM as a login manager, make sure you have <tt>numlockx</tt> installed and add :
 numlockx on
to your {{ic|/usr/share/config/kdm/Xsetup}}, or to {{ic|/opt/kde/share/config/kdm/Xsetup}} if you're using KDM3.

Note that this file lives outside of Pacman's protected area, so it might be overwritten on update without warning or creating a {{ic|.pacnew}} file. If it bothers you, add the following line to your {{ic|/etc/pacman.conf}} file (omit the leading slash in the path):

 NoUpgrade = usr/share/config/kdm/Xsetup

=== KDE4 Users ===

Go to System Settings, under the Hardware/Input Devices/Keyboard item you will find an option to select the behavior of NumLock.


====Alternate Method====
You may alternatively add a script to your {{ic|~/.kde4/Autostart}} directory:
 $ nano ~/.kde4/Autostart/numlockx
Add the following: 
 #!/bin/sh
 numlockx on
And make it executable:
 $ chmod +x ~/.kde4/Autostart/numlockx

===GDM===
First make sure that you have {{Pkg|numlockx}} (from extra) installed. Then, GDM users may add the following code to {{ic|/etc/gdm/Init/Default}}:
 if [ -x /usr/bin/numlockx ]; then
       /usr/bin/numlockx on
 fi

===SLiM===
In the file {{ic|/etc/slim.conf}} find the line:
 #numlock             on
and remove the {{Ic|#}}